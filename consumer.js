require('dotenv').config()
const Kafka = require('node-rdkafka');

function getKafkaConfig() {
    const bootstrapServers = process.env.BOOTSTRAP_SERVERS;
    if (!bootstrapServers) {
        throw new Error('Bootstrap servers variable not found');
    }
    return {
        'bootstrap.servers': bootstrapServers,
        'group.id': 'kafka-nodejs-getting-started',
    }
}

function createConsumer(config, onData) {
    const consumer = new Kafka.KafkaConsumer(config, {'auto.offset.reset': 'earliest'});

    return new Promise((resolve, reject) => {
        consumer
            .on('ready', () => resolve(consumer))
            .on('data', onData);

        consumer.connect();
    });
}


async function consumerExample() {
    const topic = process.env.KAFKA_TOPIC_NAME;

    const kafkaConfig = getKafkaConfig();
    let counter = 0;
    const consumer = await createConsumer(kafkaConfig, ({ value }) => {
        counter++;
        console.log(`Consumed event № ${counter} at ${value}`);
    });

    consumer.subscribe([topic]);
    consumer.consume();

    process.on('SIGINT', () => {
        console.log('\nDisconnecting consumer ...');
        consumer.disconnect();
    });
}

consumerExample()
    .catch((err) => {
        console.error(`Something went wrong:\n${err}`);
        process.exit(1);
    });