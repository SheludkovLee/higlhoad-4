# higlhoad-4


## Install

``` 
docker compose up -d

npm install
```

## Create Topic
```
docker compose exec broker \                                        
  kafka-topics --create \
    --topic test_kafka \
    --bootstrap-server localhost:9092 \
    --replication-factor 1 \
    --partitions 1
```

## Run
```
node producer.js

node consumer.js
```
