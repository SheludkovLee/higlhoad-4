require('dotenv').config()
const Kafka = require('node-rdkafka');

function getKafkaConfig() {
    const bootstrapServers = process.env.BOOTSTRAP_SERVERS;
    if (!bootstrapServers) {
        throw new Error('Bootstrap servers variable not found');
    }
    return {
        'bootstrap.servers': bootstrapServers,
        'dr_msg_cb': true
    }
}

function createProducer(config, onDeliveryReport) {
    const producer = new Kafka.Producer(config);

    return new Promise((resolve, reject) => {
        producer
            .on('ready', () => resolve(producer))
            .on('delivery-report', onDeliveryReport)
            .on('event.error', (err) => {
                console.warn('event.error', err);
                reject(err);
            });
        producer.connect();
    });
}

async function produceExample() {
    const kafkaConfig = getKafkaConfig();

    const topic = "test_kafka";
    const producer = await createProducer(kafkaConfig, (err, report) => {
        if (err) {
            console.warn('Error producing', err)
        } else {
            const {topic, value } = report;
            console.log(`Produced event to topic ${topic} at: ${value}`);
        }
    });

    const NUMBER_MESSAGES = 1000;

    for (let i = 0; i < NUMBER_MESSAGES; i++) {
        const currentTime = new Date();
        const message = Buffer.from(currentTime);
        producer.produce(topic, -1, message);
    }

    producer.flush(10000, () => {
        producer.disconnect();
    });
}

produceExample()
    .catch((err) => {
        console.error(`Something went wrong:\n${err}`);
        process.exit(1);
    });